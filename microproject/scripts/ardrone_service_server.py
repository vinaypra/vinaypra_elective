#! /usr/bin/env python

# Import necessary dependencies
import rospy
from std_srvs.srv import Empty, EmptyRequest, EmptyResponse
from microproject.srv import ArdroneServiceMessage, ArdroneServiceMessageRequest, ArdroneServiceMessageResponse
import sys
import time
import actionlib
from geometry_msgs.msg import Twist
from std_msgs.msg import Empty

# Initialize server node
rospy.init_node('ardrone_service_server_node')

ardrone_server_service = rospy.Service('/ardrone_maze_service', Empty , my_callback)

