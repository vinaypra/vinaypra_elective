#! /usr/bin/env python

from robot_control.rotate_robot import RobotControl
from robot_control.srv import RotateRobot, RotateRobotRequest
import rosunit
import unittest
import rostest
import rospy
from geometry_msgs.msg import Twist
import time
#import sys
PKG = 'robot_control'
NAME = 'exercise_3_5'

class MyTestSuite(unittest.TestCase):

    def setUp(self):
        self.rc = RobotControl()
        self.success = False

    def runTest_CaseA(self):

        speed, angle = self.rc.convert_degree_to_rad(60, 90)
        self.assertEquals(angle, 1.57, "1.57!=1.57")

    def runTest_CaseB(self):

        speed, angle = self.rc.convert_degree_to_rad(60, -90)
        self.assertEquals(angle, 1.57, "1.57!=1.57")

    def callback(self, msg):
        print(rospy.get_caller_id(), "Angular Speed: %s" % msg.angular.z)
        self.success = msg.angular.z and msg.angular.z == 1

    def test_publish_cmd_vel(self):

        test_sub = rospy.Subscriber("/cmd_vel", Twist, self.callback)
        self.rc.cmd.angular.z = 1
        self.rc.publish_once_in_cmd_vel()
        timeout_t = time.time() + 10.0  # 10 seconds
        while not rospy.is_shutdown() and not self.success and time.time() < timeout_t:
            time.sleep(0.1)
        self.assert_(self.success)

    def test_rotate_robot_service(self):

        rospy.wait_for_service('rotate_robot')
        s = rospy.ServiceProxy('rotate_robot', RotateRobot)
        tests = [(60, 90, 'y')]
        for x, y, z in tests:
            print("Requesting %s+%s+%s" % (x, y, z))
            # test both simple and formal call syntax
            resp = s(x, y, z)
            resp2 = s.call(RotateRobotRequest(x, y, z))
            self.assertEquals(resp.rotation_successfull,resp2.rotation_successfull)
            self.assertTrue(resp.rotation_successfull, "integration failure, service response was not True")

    def vel_callback(self, msg):

        linear_x = msg.linear.x
        angular_z = msg.angular.z

        self.assertEquals(linear_x, 0.0, "0.0!=0.0")
        self.assertEquals(angular_z, 0.0, "0.0!=0.0")

    def test_stop_robot(self):
        self.rc.stop_robot()

        test_sub_2 = rospy.Subscriber("/cmd_vel", Twist, self.vel_callback)

if __name__ == '__main__':
    rostest.rosrun(PKG, NAME, TestRobotControl)
