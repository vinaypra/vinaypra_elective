import rosunit
import rotate_robot_test_cases_name

# rosunit
rosunit.unitrun('robot_control', 'rotate_robot_test_cases',
                'rotate_robot_test_cases_name.MyTestSuite.runTest_CaseA')

rosunit.unitrun('robot_control', 'rotate_robot_test_cases',
                'rotate_robot_test_cases_name.MyTestSuite.runTest_CaseB')