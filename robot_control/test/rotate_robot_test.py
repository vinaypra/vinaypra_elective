#! /usr/bin/env python

from robot_control.rotate_robot import RobotControl
import rosunit
import unittest
import sys

# PKG: This is the name of the ROS package where these results are 
# recorded. Test results are aggregated by package name.
PKG = 'robot_control'
# NAME: This is the name to use for the test results. This name will 
# be used in the filename of the test results as well as in the XML 
# results reporting.
NAME = 'rotate_robot_test'


class TestRobotControl(unittest.TestCase):

    def setUp(self):
        self.rc = RobotControl()

    # only functions with 'test_'-prefix will be run!
    def test_deg_rad_conversion(self):

        speed, angle = self.rc.convert_degree_to_rad(60, 90)
        self.assertEquals(angle, 1.57, "1.57!=1.57")

# main condition
if __name__ == '__main__':
    rosunit.unitrun(PKG, NAME, TestRobotControl)