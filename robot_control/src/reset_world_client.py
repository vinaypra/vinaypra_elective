#! /usr/bin/env python

# Import necessary dependencies
from robot_control.rotate_robot import RobotControl
from robot_control.srv import RotateRobot, RotateRobotRequest
from std_srvs.srv import Empty, EmptyRequest
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Quaternion
from tf.transformations import euler_from_quaternion
import rospy
import time

# Initialize node
rospy.init_node('reset_world_srv_client_node')

# Wait till service is active
rospy.wait_for_service('/gazebo/reset_world')

# Service definition
delete_world_srv_client = rospy.ServiceProxy('/gazebo/reset_world', Empty)

# Service request message
delete_world_srv_client_msg = EmptyRequest()

# Pass the attributes to the service
delete_world_srv_client_call = rot_robot_srv_client(delete_world_srv_client_msg)