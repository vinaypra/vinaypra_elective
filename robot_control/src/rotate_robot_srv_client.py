#! /usr/bin/env python

# Import necessary dependencies
from robot_control.rotate_robot import RobotControl
from robot_control.srv import RotateRobot, RotateRobotRequest
from std_srvs.srv import Empty, EmptyRequest
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Quaternion
from tf.transformations import euler_from_quaternion
import rospy
import time

# Initialize node
rospy.init_node('rotate_robot_srv_client_node')

# Wait till service is active
rospy.wait_for_service('/rotate_robot')

# Service definition
rot_robot_srv_client = rospy.ServiceProxy('/rotate_robot', RotateRobot)

# Service request message
rot_robot_srv_client_msg = RotateRobotRequest()

#tests = [(60, 90, 'y')]
# From RotateRobot.srv, setting the test values
rot_robot_srv_client_msg.speed_d = 60
rot_robot_srv_client_msg.angle_d = 90
rot_robot_srv_client_msg.clockwise_yn = 'y'

# Pass the attributes to the service
rot_robot_srv_client_call = rot_robot_srv_client(rot_robot_srv_client_msg)